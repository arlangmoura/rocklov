Dado("que acesso a página de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  #cenario outline (esquema do cenario)
  user = table.hashes.first # ou pode escrever user = table.hashes[0]

  MongoDB.new.remove_user("brasil@gmail.com")

  @signup_page.create(user)
end
