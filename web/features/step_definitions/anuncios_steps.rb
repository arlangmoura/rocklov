Dado("Login com {string} e {string}") do |email, senha|
  @email = email
  @login_page.open
  @login_page.with(email, senha)
  #checkpoint para garantir que está no dashboard - feito após implementação user_id
  expect(@dash_page.on_dash?).to be true
end

Dado("que acesso o fomulário de cadastro de anúncios") do
  @dash_page.goto_equipo_form
end

Dado("que eu tenho o seguinte equipamento:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  # quando cria uma variável com @, ela vira uma variável global
  @anuncio = table.rows_hash #pega tabela de chave valor e converte para o ruby
  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
end

Quando("submeto o cadastro desse item") do
  @equipos_page.create(@anuncio)
end

Então("devo ver esse item no meu Dashboard") do
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome]
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Então("deve conter a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to have_text expect_alert
end

#Remover anuncios
Dado("que eu tenho um anúncio indesejado:") do |table|
  user_id = page.execute_script("return localStorage.getItem('user')")
  #log user_id #comando para pegar log do usuário

  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb")

  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }

  EquiposService.new.create(@equipo, user_id)

  #faz um refresh na página e atualiza para exibir o produto cadastrado.
  visit current_path
end

Quando("eu solicito a exclusão desse anúncio") do
  @dash_page.request_removal(@equipo[:name])
  sleep 1 #think time
end

Quando("confirmo a exclusão") do
  @dash_page.confirm_removal
end

Então("o item deve ser removido do dashboard") do
  expect(@dash_page.has_equipo?(@equipo[:name])).to be true
end

Quando("não confirmo a solicitação") do
  @dash_page.cancel_removal
end

Então("o item deve continuar no dashboard") do
  expect(@dash_page.has_equipo?(@equipo[:name])).to be false
  #expect(@dash_page.equipo_list).to have_content @equipo[:name] #ou pode ser dessa maneira
end
