#language: pt

@login
Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login_user
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "bread@hotmail.com" e "qabrasil"
        Então sou redirecionado para o Dashboard

    @tentar_logar
    Esquema do Cenario: Tentar logar
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input      | senha_input | mensagem_output                 |
            | brasil@gmail.com | qabrasi     | Usuário e/ou senha inválidos.   |
            | brasil@gmail.com | qabrasil    | Usuário e/ou senha inválidos.   |
            | brasil*gmail.com | qabrasil    | Oops. Informe um email válido!  |
            |                  | qabrasil    | Oops. Informe um email válido!  |
            | brasil@gmail.com |             | Oops. Informe sua senha secreta!|
