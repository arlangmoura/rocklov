#language: pt

Funcionalidade: Remover Anúncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anúncio
    Para que eu possa manter meu dashboard atualizado

    Contexto: Login
        * Login com "john@hotmail.com" e "qabrasil"

    @remover_anuncio
    Cenario: Remover um anúncio

        Dado que eu tenho um anúncio indesejado:
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 150            |
        Quando eu solicito a exclusão desse anúncio
            E confirmo a exclusão
        Então o item deve ser removido do dashboard

    @desistir_de_remover
    Cenario: Desistir de remover um anúncio

        Dado que eu tenho um anúncio indesejado:
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros    |
            | preco     | 50        |
        Quando eu solicito a exclusão desse anúncio
            Mas não confirmo a solicitação
        Então o item deve continuar no dashboard