#language: pt

Funcionalidade: Cadastro de Anúncios
    Sendo um usuário cadastrado no Rocklov que possui equipamentos musicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibiliza-los para locação

    Contexto: Login
        * Login com "nick@hotmail.com" e "qabrasil"

    @cadastro_equipo
    Cenario: Novo equipo
        Dado que acesso o fomulário de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submeto o cadastro desse item
        Então devo ver esse item no meu Dashboard

    @tentativa_anuncio
    Esquema do Cenario: Tentativa de cadastrar de anúncios
        Dado que acesso o fomulário de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | <foto>      |
            | nome      | <nome>      |
            | categoria | <categoria> |
            | preco     | <preco>     |
        Quando submeto o cadastro desse item
        Então deve conter a mensagem de alerta: "<saida>"

        Exemplos:
            | foto          | nome          | categoria | preco | saida                                |
            |               | Fender Strato | Cordas    | 200   | Adicione uma foto no seu anúncio!    |
            | fender-sb.jpg |               | Cordas    | 200   | Informe a descrição do anúncio!      |
            | fender-sb.jpg | Fender Strato |           | 200   | Informe a categoria                  |
            | fender-sb.jpg | Fender Strato | Cordas    |       | Informe o valor da diária            |
            | conga.jpg     | Conga         | Outros    | abc   | O valor da diária deve ser numérico! |
            | conga.jpg     | Conga         | Outros    | 100a  | O valor da diária deve ser numérico! |