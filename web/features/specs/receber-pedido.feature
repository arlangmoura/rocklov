#language: pt

Funcionalidade: Receber pedido de locação
    Sendo um anunciante que possui equipamentos cadastrado
    Desejo receber pedidos de locação
    Para que eu possa decidir se quero aprova-los ou rejeita-los

    @receber_pedido
    Cenario: Receber pedido

        Dado que meu perfil de anunciante é "frank@hotmail.com" e "qabrasil"
            E que eu tenho o seguinte equipamento cadastrado:
            | thumb     | trompete.jpg |
            | nome      | Trompete     |
            | categoria | Outros       |
            | preco     | 100          |
            E acesso o meu dashboard
        Quando  "mary@hotmail.com" e "qabrasil" solicita a locação desse equipo
        Então devo ver a seguinte mensagem:
            """
            mary@hotmail.com deseja alugar o equipamento: Trompete em: DATA_ATUAL
            """
            E devo ver os links: "ACEITAR" e "REJEITAR" no pedido