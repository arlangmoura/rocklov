#language: pt

@cadastro
Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @fazer_cadastro
    Cenario: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome     | email            | senha    |
            | qabrasil | brasil@gmail.com | qabrasil |

        Então sou redirecionado para o Dashboard

    @tentativa_cadastro
    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input | email_input      | senha_input | mensagem_output                  |
            |            | brasil@gmail.com | qabrasil    | Oops. Informe seu nome completo! |
            | qabrasil   |                  | qabrasil    | Oops. Informe um email válido!   |
            | qabrasil   | brasil#gmail.com | qabrasil    | Oops. Informe um email válido!   |
            | qabrasil   | brasil@gmail.com |             | Oops. Informe sua senha secreta! |
