require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"

#Aqui é uma constante, diferente de variável, constante não muda valor
CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))

=begin 
BROWSER = ENV["BROWSER"]

if BROWSER == "firefox"
  @driver = :selenium
elsif BROWSER == "firefox_headless"
  @driver = :selenium_headless
elsif BROWSER == "chrome"
  @driver = :selenium_chrome
else
  @driver = :selenium_chrome_headless
end 
=end

#case ou switchcase
#log serve para steps definitions, puts tem o mesmo efeito, raise estoura uma excessão
case ENV["BROWSER"]
when "firefox"
  @driver = :selenium
when "chrome"
  @driver = :selenium_chrome
when "firefox_headless"
  @driver = :selenium_headless
when "chrome_headless"

  #código fonte retirado do git capybara para rodar o pipeline em headless
  Capybara.register_driver :selenium_chrome_headless do |app|
    version = Capybara::Selenium::Driver.load_selenium
    options_key = Capybara::Selenium::Driver::CAPS_VERSION.satisfied_by?(version) ? :capabilities : :options
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.add_argument("--headless")
      opts.add_argument("--disable-gpu") #if Gem.win_platform?
      opts.add_argument("--disable-site-isolation-trials")
      #argumentos acrescentados para funcionar image 'arlangmoura/ruby-web-agent'
      opts.add_argument("--no-sandbox")
      opts.add_argument("--disable-dev-shm-usage") #serve para usar cash em disco em vez de memória
    end

    Capybara::Selenium::Driver.new(app, **{ :browser => :chrome, options_key => browser_options })
  end

  @driver = :selenium_chrome_headless
else
  raise "Navegador incorreto, variável @driver está vazia."
end

Capybara.configure do |config|
  #:selenium para firefox ou :selenium_chrome para chrome
  #:selenium_headless ou :selenium_chrome_headless - execução sem apresentar o browser
  config.default_driver = @driver #:selenium_chrome_headless - método antigo
  config.app_host = CONFIG["url"] #"http://rocklov-web:3000" #padroniza a url do sistema(forma antiga).
  config.default_max_wait_time = 10 #sistema tem até 10 segundo para responder.
end

AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  config.clean_results_directory = true
end
