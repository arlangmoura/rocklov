require "mongo" #importanto biblioteca

#criando um arquivo de log (na pasta criada logs)
Mongo::Logger.logger = Logger.new("./logs/mongo.log")

class MongoDB
  attr_accessor :users, :equipos, :client

  def initialize
    @client = Mongo::Client.new(CONFIG["mongo"]) #("mongodb://rocklov-db:27017/rocklov") modo antigo
    @users = client[:users]
    @equipos = client[:equipos]
  end

  # utilizado no processo semente com rake
  def drop_danger
    @client.database.drop
  end

  def insert_users(docs)
    @users.insert_many(docs)
  end

  #---------------------------------------
  def remove_user(email)
    #esse código serve para acessar e apagar o user no mongo db
    #client = Mongo::Client.new('mongodb://rocklov-db:27017/rocklov')
    #users = client[:users]
    @users.delete_many({ email: email })
  end

  def get_user(email)
    #client = Mongo::Client.new('mongodb://rocklov-db:27017/rocklov')
    #users = client[:users]
    user = @users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, email)
    user_id = get_user(email)
    #client = Mongo::Client.new('mongodb://rocklov-db:27017/rocklov')
    #equipos = client[:equipos]
    @equipos.delete_many({ name: name, user: user_id })
  end
end

#MongoDB.new.get_user("brasil@yahoo.com")
