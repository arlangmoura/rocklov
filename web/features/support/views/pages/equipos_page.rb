class EquiposPage
  include Capybara::DSL

  def create(equipo)
    # código aplicado em anuncios_steps, proibído usar em page object
    # era um checkpoint para garantir que estava no lugar correto
    # expect(page).to have_css "equipoform" #substituído pelo código abaixo.

    # checkpoint com timeout explícito
    page.has_css?("#equipoForm") # a ? retorna verdadeiro ou falso

    upload(equipo[:thumb]) if equipo[:thumb].length > 0 # opção apenas chamada se tiver valor

    # $ se termina com equipamento
    # * se contém a palavra "equipamento"
    # ^ se começa com equipamento
    find("input[placeholder$=equipamento]").set equipo[:nome]
    select_cat(equipo[:categoria]) if equipo[:categoria].length > 0 # opção apenas chamada se tiver valor
    find("input[placeholder^=Valor]").set equipo[:preco]

    click_button "Cadastrar"
  end

  def select_cat(cat)
    find("#category").find("option", text: cat).select_option
  end

  def upload(file_name)
    thumb = Dir.pwd + "/features/support/fixtures/images/" + file_name

    find("#thumbnail input[type=file]", visible: false).set thumb
  end
end
