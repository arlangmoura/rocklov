Before do
  @alert = Alert.new
  @login_page = LoginPage.new
  @signup_page = SignupPage.new
  @dash_page = DashPage.new
  @equipos_page = EquiposPage.new

  page.driver.browser.manage.window.maximize #maximiza a tela do navegador
  #page.current_window.resize_to(1280, 720) #padroniza um tamanho específico
end

After do
  #tira uma screenshot depois do passo 'então'
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")
  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),
  )
end
