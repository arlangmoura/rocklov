require_relative "base_api"

class Equipos < BaseApi
  def create(payload, user_id)
    return self.class.post(
             "/equipos",
             body: payload, # form-data não é do tipo json, não precisa fazer conversão como singup e sessions.
             headers: {
               #"Content-Type": "application/json",#não utilizado porque é um form-data, não trabalha com json
               "user_id": user_id,
             },
           )
  end

  # nome de método "find" encapsula uma chamada "get"
  # não recomenda mudar para get porque pode trazer alguma confusão com o framework do httparty
  def find_by_id(equipo_id, user_id)
    return self.class.get(
             "/equipos/#{equipo_id}",
             headers: {
               "user_id": user_id,
             },
           )
  end

  def remove_by_id(equipo_id, user_id)
    return self.class.delete(
             "/equipos/#{equipo_id}",
             headers: {
               "user_id": user_id,
             },
           )
  end

  def list(user_id)
    return self.class.get(
             "/equipos",
             headers: {
               "user_id": user_id,
             },
           )
  end

  def booking(equipo_id, user_locator_id)
    return self.class.post(
             "/equipos/#{equipo_id}/bookings",
             body: { date: Time.now.strftime("%d/%m/%Y") }.to_json,
             headers: {
               "user_id": user_locator_id,
             },
           )
  end
end
