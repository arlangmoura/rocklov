#a importação foi configurada no arquivo spec_helper.rb
#require_relative "routes/signup"
#require_relative "libs/mongo"

describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Brasil", email: "brasil@uol.com.br", password: "qabrasil" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado que eu tenho um novo usuário
      payload = { name: "Teste Qa", email: "tqa@uol.com.br", password: "qabrasil" }
      MongoDB.new.remove_user(payload[:email])
      # e o email desse usuário já foi cadastrado no sistema
      Signup.new.create(payload)
      # quando faço a requisição para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  context "name obrigatório" do
    before(:all) do
      payload = { name: "", email: "brasil@uol.com.br", password: "qabrasil" }
      @result = Signup.new.create(payload)
    end

    it "deve retornar 412" do
      expect(@result.code).to eql 412
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "required name"
    end
  end

  context "email obrigatório" do
    before(:all) do
      payload = { name: "Brasil", email: "", password: "qabrasil" }
      @result = Signup.new.create(payload)
    end

    it "deve retornar 412" do
      expect(@result.code).to eql 412
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "required email"
    end
  end

  context "password é obrigatório" do
    before(:all) do
      payload = { name: "Brasil", email: "brasil@uol.com.br", password: "" }
      @result = Signup.new.create(payload)
    end

    it "deve retornar 412" do
      expect(@result.code).to eql 412
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "required password"
    end
  end
end
