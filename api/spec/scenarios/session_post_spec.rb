#a importação foi configurada no arquivo spec_helper.rb
#require_relative "routes/sessions"
#require_relative "helpers"

describe "POST /sessions" do
  context "login com sucesso" do
    #:all é o before que roda uma única vez para todos os exemplos
    before(:all) do
      payload = { email: "robert@hotmail.com", password: "qabrasil" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      #conta a quantidade de caracteres. No mongo db são 24.
      expect(@result.parsed_response["_id"].length).to eql 24
      #puts result.parsed_response["_id"]
      #puts result.parsed_response.class
    end
  end

=begin   
  examples = [
    {
      title: "senha incorreta",
      payload: { email: "brasil@yahoo.com", password: "qa1234" },
      code: 401,
      error: "Unauthorized",
    },
    {
      title: "usuario nao existe",
      payload: { email: "404@yahoo.com", password: "qa1234" },
      code: 401,
      error: "Unauthorized",
    },
    {
      title: "email em branco",
      payload: { email: "", password: "qa1234" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem o campo email",
      payload: { password: "qa1234" },
      code: 412,
      error: "required email",
    },
    {
      title: "senha em branco",
      payload: { email: "brasil@yahoo.com", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem o campo senha",
      payload: { email: "brasil@yahoo.com" },
      code: 412,
      error: "required password",
    },
  ] 
=end
  #puts examples.to_json

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        #payload = { email: "brasil@yahoo.com", password: "qa1234" }
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
