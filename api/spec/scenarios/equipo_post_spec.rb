#a importação foi configurada no arquivo spec_helper.rb
#require_relative "routes/equipos"
#require_relative "routes/sessions"

describe "POST /equipos" do
  before(:all) do
    payload = { email: "richard@hotmail.com", password: "qabrasil" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "novo equipo" do
    before(:all) do
      thumbnail = Helpers::get_thumb("kramer.jpg")

      payload = {
        thumbnail: thumbnail,
        name: "Kramer",
        category: "Cordas",
        price: 299,
      }

      MongoDB.new.remove_equipo(payload[:name], @user_id)

      @result = Equipos.new.create(payload, @user_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end

  context "não autrizado" do
    before(:all) do
      payload = {
        thumbnail: Helpers::get_thumb("baixo.jpg"),
        name: "Contra Baixo",
        category: "Cordas",
        price: 59,
      }

      MongoDB.new.remove_equipo(payload[:name], @user_id)

      @result = Equipos.new.create(payload, nil) #nil representa nulo no ruby
    end

    it "deve retornar 401" do
      expect(@result.code).to eql 401
    end
  end
end
