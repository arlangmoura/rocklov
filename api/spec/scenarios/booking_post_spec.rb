describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "penelope@hotmail.com", password: "qabrasil" }
    result = Sessions.new.login(payload)
    @brasil_id = result.parsed_response["_id"]
  end

  context "solicitar locacao" do
    before(:all) do
      # dado que "tqa@uol.com.br" tem um equipamento para locação
      result = Sessions.new.login({ email: "margoh@hotmail.com", password: "qabrasil" })
      tqa_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        name: "Fender",
        category: "Cordas",
        price: 300,
      }

      MongoDB.new.remove_equipo(fender[:name], tqa_id)

      result = Equipos.new.create(fender, tqa_id)
      fender_id = result.parsed_response["_id"]

      # quando solicito a locação do equipamento de "tqa@uol.com.br"
      @result = Equipos.new.booking(fender_id, @brasil_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
