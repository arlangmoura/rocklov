require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"

require_relative "libs/mongo"
require_relative "helpers"

require "digest/md5" #criptografia de senha tipo md5

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

# se retirar/mover o spec_helper da raiz do projeto, dará erro no projeto
RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  # aqui adiciona contas que serão utilizadas nos cenários, mesmo após a atualização de versão
  # isso chama processo semente, pode cadastrar mais de 1 user
  config.before(:suite) do
    users = [
      { name: "Robert Silver", email: "robert@hotmail.com", password: to_md5("qabrasil") },
      { name: "Richard Bennet", email: "richard@hotmail.com", password: to_md5("qabrasil") },
      { name: "Thomas Wayne", email: "thomas@hotmail.com", password: to_md5("qabrasil") },
      { name: "Penelope Cruz", email: "penelope@hotmail.com", password: to_md5("qabrasil") },
      { name: "Margoh Perkins", email: "margoh@hotmail.com", password: to_md5("qabrasil") },
    ]

    MongoDB.new.drop_danger
    MongoDB.new.insert_users(users)
  end
end
